//
//  CalcultorPresenter.swift
//  PracticaTestUnitarios
//
//  Created by formador on 9/5/19.
//  Copyright © 2019 formador. All rights reserved.
//

import Foundation

class CalculatorPresenter {
    
    private var dataManager: DataManager?
    private var serviceManager: ServiceManager?

    func setUp(dataManager: DataManager, serviceManager: ServiceManager) {
        
        self.dataManager = dataManager
        self.serviceManager = serviceManager
    }
    
    func addValues(one: Int, two: Int) -> Double {
        
        let result = Double(one + two)
        dataManager?.saveResult(result)
        serviceManager?.sendResult(result)
        
        return result
    }
    
    func substractValues(one: Int, two: Int) -> Double {
        
        let result = Double(one - two)
        dataManager?.saveResult(result)
        serviceManager?.sendResult(result)
        
        return result
    }
    
    func multiplyValues(one: Int, two: Int) -> Double {
        
        let result = Double(one * two)
        dataManager?.saveResult(result)
        serviceManager?.sendResult(result)
        
        return result
    }
    
    func divideValues(one: Int, two: Int) -> Double {
        
        let result = Double(one / two)
        dataManager?.saveResult(result)
        serviceManager?.sendResult(result)
        
        return result
    }
}
