//
//  ViewController.swift
//  PracticaTestUnitarios
//
//  Created by formador on 9/5/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var firstValueTextField: UITextField!
    @IBOutlet weak var secondValueTextField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    
    var presenter: CalculatorPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let bar = UIToolbar()
        
        let add = UIBarButtonItem(title: "+", style: .plain, target: self, action: #selector(addTapped))
        let subtract = UIBarButtonItem(title: "-", style: .plain, target: self, action: #selector(subtractTapped))
        let multiply = UIBarButtonItem(title: "*", style: .plain, target: self, action: #selector(multyplyTapped))
        let divide = UIBarButtonItem(title: "/", style: .plain, target: self, action: #selector(divideTapped))
        
        bar.items = [add, subtract, multiply, divide]
        bar.sizeToFit()
        firstValueTextField.inputAccessoryView = bar
        secondValueTextField.inputAccessoryView = bar
    }

    fileprivate func dissmissKeyboard() {
        
        firstValueTextField.resignFirstResponder()
        secondValueTextField.resignFirstResponder()
    }

    @objc private func addTapped() {
        
        guard let valueOneString = firstValueTextField.text, let valueTwoString = secondValueTextField.text, let valueOne = Int(valueOneString), let valueTwo = Int(valueTwoString) else {
            dissmissKeyboard()
             return
        }
        
        if let result = presenter?.addValues(one: valueOne, two: valueTwo) {
            resultLabel.text = "\(result)"
        }
        
        
       dissmissKeyboard()
    }
    
    @objc private func subtractTapped() {
        
        guard let valueOneString = firstValueTextField.text, let valueTwoString = secondValueTextField.text, let valueOne = Int(valueOneString), let valueTwo = Int(valueTwoString) else {
            dissmissKeyboard()
            return
        }
        
        if let result = presenter?.substractValues(one: valueOne, two: valueTwo) {
            resultLabel.text = "\(result)"
        }
        
        dissmissKeyboard()
    }

    @objc private func multyplyTapped() {
        
        guard let valueOneString = firstValueTextField.text, let valueTwoString = secondValueTextField.text, let valueOne = Int(valueOneString), let valueTwo = Int(valueTwoString) else {
            dissmissKeyboard()
            return
        }
        
        if let result = presenter?.multiplyValues(one: valueOne, two: valueTwo) {
            resultLabel.text = "\(result)"
        }
        
        dissmissKeyboard()
    }
    
    @objc private func divideTapped() {
        
        guard let valueOneString = firstValueTextField.text, let valueTwoString = secondValueTextField.text, let valueOne = Int(valueOneString), let valueTwo = Int(valueTwoString) else {
            dissmissKeyboard()
            return
        }
        
        if let result = presenter?.divideValues(one: valueOne, two: valueTwo) {
            resultLabel.text = "\(result)"
        }
        
        dissmissKeyboard()
    }
}

