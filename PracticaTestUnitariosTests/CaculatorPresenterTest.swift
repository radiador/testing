//
//  CaculatorPresenterTest.swift
//  PracticaTestUnitariosTests
//
//  Created by formador on 9/5/19.
//  Copyright © 2019 formador. All rights reserved.
//

import XCTest
@testable import PracticaTestUnitarios

class CaculatorPresenterTest: XCTestCase {
    
    var calulatorPresenter: CalculatorPresenter!
    var dummyDataManager: DummyDataManager!
    var dummyServiceManager: DummyServiceManager!
    
    override func setUp() {
        
        calulatorPresenter = CalculatorPresenter()
        
        dummyDataManager = DummyDataManager()
        dummyServiceManager = DummyServiceManager()

        calulatorPresenter.setUp(dataManager: dummyDataManager, serviceManager: dummyServiceManager)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    //MARK: ADD
    func testGivenTwoValuesWhenICallAddValuesThenIReturnRightValue() {
        
        //Given
        let valueOne = 10
        let valueTwo = 20
    
        //When
        let result = calulatorPresenter.addValues(one: valueOne, two: valueTwo)

        //Then
        XCTAssert(result == 30)
    }
    
    func testGivenTwoValuesWhenICallAddValuesThenICallDataManagerSaveResult() {
        
        //Given
        let valueOne = 10
        let valueTwo = 20
        
        //When
        _ = calulatorPresenter.addValues(one: valueOne, two: valueTwo)

        XCTAssert(dummyDataManager.isCalledSaveResult)
    }
    
    func testGivenTwoValuesWhenICallAddValuesThenICallDataManagerSaveResultWithRightValue() {
        
        //Given
        let valueOne = 10
        let valueTwo = 20
        
        //When
        let result = calulatorPresenter.addValues(one: valueOne, two: valueTwo)

        XCTAssert(dummyDataManager.result == result)
    }
    
    func testGivenTwoValuesWhenICallAddValuesThenICallServiceManagerSendResult() {
        
        //Given
        let valueOne = 10
        let valueTwo = 20
        
        //When
        _ = calulatorPresenter.addValues(one: valueOne, two: valueTwo)
        
        XCTAssert(dummyServiceManager.isCalledSendResult)
    }
    
    func testGivenTwoValuesWhenICallAddValuesThenICallServiceManagerSendResultWithRightValue() {
        
        //Given
        let valueOne = 10
        let valueTwo = 20
        
        //When
        let result = calulatorPresenter.addValues(one: valueOne, two: valueTwo)
        
        XCTAssert(dummyServiceManager.result == result)
    }
    
    //MARK: SUBTRACT
    func testGivenTwoValuesWhenICallSubtractValuesThenIReturnRightValue() {
        
        //Given
        let valueOne = 20
        let valueTwo = 5
        
        //When
        let result = calulatorPresenter.substractValues(one: valueOne, two: valueTwo)
        
        //Then
        XCTAssert(result == 15)
    }
    
    func testGivenTwoValuesWhenICallSubtractValuesThenICallDataManagerSaveResult() {
        
        //Given
        let valueOne = 10
        let valueTwo = 20
        
        //When
        _ = calulatorPresenter.substractValues(one: valueOne, two: valueTwo)
        
        XCTAssert(dummyDataManager.isCalledSaveResult)
    }
    
    func testGivenTwoValuesWhenICallSubtractValuesThenICallDataManagerSaveResultWithRightValue() {
        
        //Given
        let valueOne = 10
        let valueTwo = 20
        
        //When
        let result = calulatorPresenter.substractValues(one: valueOne, two: valueTwo)
        
        XCTAssert(dummyDataManager.result == result)
    }
    
    func testGivenTwoValuesWhenICallSubtractValuesThenICallServiceManagerSendResult() {
        
        //Given
        let valueOne = 10
        let valueTwo = 20
        
        //When
        _ = calulatorPresenter.substractValues(one: valueOne, two: valueTwo)
        
        XCTAssert(dummyServiceManager.isCalledSendResult)
    }
    
    func testGivenTwoValuesWhenICallSubtractValuesThenICallServiceManagerSendResultWithRightValue() {
        
        //Given
        let valueOne = 10
        let valueTwo = 20
        
        //When
        let result = calulatorPresenter.substractValues(one: valueOne, two: valueTwo)
        
        XCTAssert(dummyServiceManager.result == result)
    }
    
    //MARK: MUTIPLY
    func testGivenTwoValuesWhenICallMultiplyValuesThenIReturnRightValue() {
        
        //Given
        let valueOne = 10
        let valueTwo = 5
        
        //When
        let result = calulatorPresenter.multiplyValues(one: valueOne, two: valueTwo)
        
        //Then
        XCTAssert(result == 50)
    }
    
    func testGivenTwoValuesWhenICallMultiplyValuesThenICallDataManagerSaveResult() {
        
        //Given
        let valueOne = 10
        let valueTwo = 20
        
        //When
        _ = calulatorPresenter.multiplyValues(one: valueOne, two: valueTwo)
        
        XCTAssert(dummyDataManager.isCalledSaveResult)
    }
    
    func testGivenTwoValuesWhenICallMultiplyValuesThenICallDataManagerSaveResultWithRightValue() {
        
        //Given
        let valueOne = 10
        let valueTwo = 20
        
        //When
        let result = calulatorPresenter.multiplyValues(one: valueOne, two: valueTwo)
        
        XCTAssert(dummyDataManager.result == result)
    }
    
    func testGivenTwoValuesWhenICallMultiplyValuesThenICallServiceManagerSendResult() {
        
        //Given
        let valueOne = 10
        let valueTwo = 20
        
        //When
        _ = calulatorPresenter.multiplyValues(one: valueOne, two: valueTwo)
        
        XCTAssert(dummyServiceManager.isCalledSendResult)
    }
    
    func testGivenTwoValuesWhenICallMultiplyValuesThenICallServiceManagerSendResultWithRightValue() {
        
        //Given
        let valueOne = 10
        let valueTwo = 20
        
        //When
        let result = calulatorPresenter.multiplyValues(one: valueOne, two: valueTwo)
        
        XCTAssert(dummyServiceManager.result == result)
    }
    
    //MARK: DIVIDE
    func testGivenTwoValuesWhenICallDivideValuesThenIReturnRightValue() {
        
        //Given
        let valueOne = 10
        let valueTwo = 5
        
        //When
        let result = calulatorPresenter.divideValues(one: valueOne, two: valueTwo)
        
        //Then
        XCTAssert(result == 2)
    }
    
    func testGivenTwoValuesWhenICallDivideValuesThenICallDataManagerSaveResult() {
        
        //Given
        let valueOne = 10
        let valueTwo = 20
        
        //When
        _ = calulatorPresenter.divideValues(one: valueOne, two: valueTwo)
        
        XCTAssert(dummyDataManager.isCalledSaveResult)
    }
    
    func testGivenTwoValuesWhenICallDivideValuesThenICallDataManagerSaveResultWithRightValue() {
        
        //Given
        let valueOne = 10
        let valueTwo = 20
        
        //When
        let result = calulatorPresenter.divideValues(one: valueOne, two: valueTwo)
        
        XCTAssert(dummyDataManager.result == result)
    }
    
    func testGivenTwoValuesWhenICallDivideValuesThenICallServiceManagerSendResult() {
        
        //Given
        let valueOne = 10
        let valueTwo = 20
        
        //When
        _ = calulatorPresenter.divideValues(one: valueOne, two: valueTwo)
        
        XCTAssert(dummyServiceManager.isCalledSendResult)
    }
    
    func testGivenTwoValuesWhenICallDivideValuesThenICallServiceManagerSendResultWithRightValue() {
        
        //Given
        let valueOne = 10
        let valueTwo = 20
        
        //When
        let result = calulatorPresenter.divideValues(one: valueOne, two: valueTwo)
        
        XCTAssert(dummyServiceManager.result == result)
    }

    //MARK: Dummy class

    class DummyDataManager: DataManager {
        
        var isCalledSaveResult = false
        var result: Double = -1
        
        override func saveResult(_ result: Double) {
            
            isCalledSaveResult = true
            self.result = result
        }
    }
    
    class DummyServiceManager: ServiceManager {
        
        var isCalledSendResult = false
        var result: Double = -1

        override func sendResult(_ result: Double) {
            
            isCalledSendResult = true
            self.result = result
        }
    }
    
}
